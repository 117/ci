package ci

import (
	"fmt"
	"log"
	"os/exec"
	"time"

	"github.com/teamwork/reload"
)

// Every ...
func Every(interval time.Duration) {
	go func() {
		if err := reload.Do(log.Printf); err != nil {
			panic(err)
		}
	}()

	go func() {
		first := true

		for {
			if !first {
				time.Sleep(interval)
			}

			first = false

			pull := exec.Command("git", "pull")
			result, err := pull.CombinedOutput()
			output := string(result)

			if err != nil {
				fmt.Printf(output)
				continue
			}
		}
	}()
}
